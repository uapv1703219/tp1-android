package com.ceri.uapv1703219.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    final static CountryList countrylist = new CountryList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*final ListView listview = findViewById(R.id.listCountry);

        final ArrayAdapter<String> adapter =new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, CountryList.getNameArray());
        listview.setAdapter(adapter);*/

        final RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerView);

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new IconicAdapter());
    }

    class IconicAdapter extends RecyclerView.Adapter<IconicAdapter.RowHolder> {
        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false)));
        }

        @Override
        public void onBindViewHolder(final RowHolder holder, int position) {
            holder.bindModel(countrylist.getNameArray()[position]);
            holder.itemView.setOnClickListener(new View.OnClickListener()
            {

                @Override
                public void onClick(View v) {
                    final String item = (String) holder.label.getText();
                    Intent myIntent = new Intent(MainActivity.this, CountryActivity.class);
                    myIntent.putExtra("country", item);
                    startActivity(myIntent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return (countrylist.getNameArray().length);
        }

        class RowHolder extends RecyclerView.ViewHolder {
            TextView label = null;
            ImageView icon = null;

            RowHolder(View row) {
                super(row);

                label = (TextView) row.findViewById(R.id.textView);
                icon = (ImageView) row.findViewById(R.id.imageView);
            }

            void bindModel(String item) {
                label.setText(item);
                icon.setImageResource(getResources().getIdentifier(CountryList.getCountry(label.getText().toString()).getmImgFile(), "drawable", getPackageName()));

            }

        }
    }
}
